
#1.The members details arranged in decreasing order of Date of Birth
   
   SELECT * FROM members ORDER BY date_of_birth DESC

#2.The members details sorting using two columns(gender,date_of_birth); the first one is sorted in ascending order by default while the second column is sorted in descending order
  
   SELECT * FROM members ORDER BY gender ASC,date_of_birth  DESC

#3.The members details get the unique values for genders
	
	SELECT DISTINCT gender FROM members;

#4.The movies detials get a list of movie category_id  and corresponding years in which they were released
	
	SELECT `category_id`,`year_released` FROM `movies` ; 

#5.The members table - get total number of males and females
	
	Select count(gender),gender from EMPLOYEE group by gender

#6.The movies table - get all the movies that have the word "code" as part of the title, we would use the percentage wildcard to perform a pattern match on both sides of the word "code".
	
	SELECT * FROM movies WHERE title LIKE '%code%';

#7.The movies table - get all the movies that were released in the year "200X" (using _ underscore wildcard)
	
	SELECT * FROM movies WHERE year_released LIKE '200_';

#8.The movies table - get movies that were not released in the year 200x
	
	SELECT * FROM movies WHERE year_released NOT LIKE '200_';

#9.The movies table  - movie titles in upper case letters
	
	UPDATE movies SET title = UPPER(title)

#10.Create new table "movierentals" (see movierentals.png image)
	
	CREATE TABLE movierentals (
		reference_number int(3) primary key,
		transction_date date,
		return_date date,
		membership_number int(5),
		movie_id int(10),
		movie_returner int(3)

	);

#11.The movierentals table - get the number of times that the movie with id 2 has been rented out
	
	SELECT COUNT(movie_id),movie_id from movierentals GROUP BY movie_id HAVING movie_id='2'

#12.The movierentals table - omits duplicate records which have same movie_id
	
	SELECT DISTINCT movie_id FROM movierentals;

#13.The movie table - latest movie year released(using MAX function)
	
	SELECT MAX(year_released) FROM movies

#14.Create "payments" table (see payments.png image)

	CREATE TABLE payments(
		payment_id int(3) primary key,
		membership_number int(5),
		payment_date date,
		description varchar(255),
		amount_paid int(10),
		external_reference_number int(5)

	);

#15.The payments table - find the average amount paid
	
	SELECT AVG(amount_paid) FROM payments

#16.Add a new field to the members table
	
	ALTER TABLE members ADD COLUMN credit_card_number VARCHAR(25);

#17.Delete a database from MySQL server (DROP command is used )
	
	DROP DATABASE assi_1;

#18.Delete an object (like Table , Column)from a database.(DROP command is used )

	ALTER TABLE payments   DROP  COLUMN payment_id;

#19.DROP a table from Database
	
	DROP table movies;

#20.Rename table `movierentals` TO `movie_rentals`;
	
	ALTER TABLE movierentals RENAME TO movie_rentals;

#21.The member table  - changes the width of "fullname" field from 250 to 50
	
	ALTER TABLE members MODIFY 'full_names' char(50) NOT NULL;

#22.The member table  - Getting a list of ten (10) members only from the database
	
	SELECT *  FROM members LIMIT 10;

#23.The member table - gets data starting the second row and limits the results to 2
	
	SELECT *  FROM members LIMIT 2,4;


